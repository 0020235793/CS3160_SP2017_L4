﻿//Description: This is a game of tic tac toe in order to show knowledge 
//              of classes and drawing with graphics
//Auhtor: Barich, Alex
//Class: CS3160 Spring 2017 Sect 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
        //Create base shape and game objects to universalize
        //graphics handler and coordinate tracking
        Shape shape = new Shape();
        Game game = new Game();

        public Form1()
        {
            InitializeComponent();
        }

        //reset tool button
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //create dummy shape based off of game grid
            //to dispose of all shape objects within the grid
            Shape[,] s = game.GetGrid();

            //traverse through all objects and dispose
            foreach(Shape i in s)
            {
                if (i != null)
                {
                    i.Dispose();
                }
            }
            //dispose of of old shape object and create new game and
            //and shape objects. Reload the form with current object
            //sender and events. Invalidate the page to redraw the
            //playing field and reset the label
            shape.Dispose();
            game = new Game();
            shape = new Shape();
            Form1_Load(sender, e);
            this.Invalidate();
            lbTurn.Text = "O's Turn";
        }

        //exit menu
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //on load
        private void Form1_Load(object sender, EventArgs e)
        {
            //create a graphics handler and store it in the shape getter/setter
            Graphics g = CreateGraphics();
            this.BackColor = Color.White;
            

            shape.graphics = g;
        }

        //paint form
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //Points used to create the Hash for the playing.
            //A pen object is then created to draw
            Point line1 = new Point(100, 100);
            Point line1end = new Point(100, 350);
            Point line2 = new Point(200,100);
            Point line2end = new Point(200,350);
            Point line3 = new Point(25, 175);
            Point line3end = new Point(275,175);
            Point line4 = new Point(25, 275);
            Point line4end = new Point(275, 275);

            Pen pen = new Pen(Color.Black, 3);
            e.Graphics.DrawLine(pen, line1.X, line1.Y, line1end.X, line1end.Y);
            e.Graphics.DrawLine(pen, line2.X, line2.Y, line2end.X, line2end.Y);
            e.Graphics.DrawLine(pen, line3.X, line3.Y, line3end.X, line3end.Y);
            e.Graphics.DrawLine(pen, line4.X, line4.Y, line4end.X, line4end.Y);

            //create dummy Coordinates and grids.
            //Traverse Shape array to determine where
            //a spot should be drawn based on previous coordinates
            Coordinates[,] c = game.GetCoordinates();
            Shape[,] s = game.GetGrid();

            for (int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    if(s[i,j] != null)
                    {
                        s[i, j].Draw(c[i, j].getC());
                    }
                }
            }
            //check if there is a tie or a winner
            if(game.IsGameOver())
            {
                if(game.isWin())
                {
                    if(lbTurn.Text == "O's Turn")
                    {
                        lbTurn.Text = "O is Winner";
                    }
                    else if (lbTurn.Text == "X's Turn")
                    {
                        lbTurn.Text = "X is Winner";
                    }
                }
                else
                {
                    lbTurn.Text = "It's a Tie";
                }
            }
            if (game.isWin())
            {
                if (lbTurn.Text == "O's Turn")
                {
                    lbTurn.Text = "O is Winner";
                }
                else if (lbTurn.Text == "X's Turn")
                {
                    lbTurn.Text = "X is Winner";
                }
            }
        }

        //mouse click event
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            //redundancy with checking for a win, but useful for minor cases
            //otherwise checking which turn it is and creating a shape
            //object based on the label text. Invalidate aftwards to
            //redraw the playing field
            if (!game.isWin())
            {
                if (lbTurn.Text == "O's Turn")
                {
                    Shape a = new OShape();
                    a.graphics = shape.graphics;
                    game.SetGrid(e.X, e.Y, a);
                    this.Invalidate();
                    if (!game.isWin())
                    {
                        lbTurn.Text = game.NextMove();
                    }
                    else
                    {
                        lbTurn.Text = "Play Again?";
                    }
                }
                else if(lbTurn.Text == "X's Turn")
                {
                    Shape a = new XShape();
                    a.graphics = shape.graphics;
                    game.SetGrid(e.X, e.Y, a);
                    this.Invalidate();
                    if (!game.isWin())
                    {
                        lbTurn.Text = game.NextMove();
                    }
                    else
                    {
                        lbTurn.Text = "Play Again?";
                    }
                }
            }



        }
    }
}
