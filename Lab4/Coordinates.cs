﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    class Coordinates
    {
        private int v1;
        private int v2;

        public Coordinates(int v1, int v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
        public Point getC()
        {
            Point p = new Point(v1, v2);
            return p;
        }
    }
}
