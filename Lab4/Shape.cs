﻿//Description: Shape and X/O classes to handle the graphics and drawing
//Auhtor: Barich, Alex
//Class: CS3160 Spring 2017 Sect 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    class Shape
    {
        //pen object to be used for drawing
        private Pen pen;
        //function name: graphics
        //description: graphics getter and setter
        //parameters: none
        //return value: Graphics
        public Graphics graphics
        {
            get; set;
        }
        //function name:Shape
        //description: constructor to create new pen object
        //parameters: none
        //return value: none
        public Shape()
        {
            pen = new Pen(Color.Gray, 3);
        }
        //function name: Dispose
        //description: disposes of pen object
        //parameters: none
        //return value: void
        public void Dispose()
        {
            pen.Dispose();
        }
        //return own pen object
        public Pen getPen() { return pen; }
        //function name: Draw
        //description: Virtual function to be called by child classes
        //parameters: Point
        //return value: void
        public virtual void Draw(Point a)
        {

        }


    }
    //OShape class inherit from Shape
    class OShape : Shape
    {
        //constructor
        public OShape() : base()
        {
        }
        //function name: Draw
        //description: overriden function to draw and ellipse
        //parameters: Point
        //return value: void
        public override void Draw(Point a)
        {
            Graphics g = graphics;

            g.DrawEllipse(this.getPen(), a.X, a.Y, 75, 75);
        }
    }
    //XShape class inherited from Shape
    class XShape : Shape
    {
        //constructor
        public XShape() : base()
        {
        }
        //function name: Draw
        //description: overriden function to draw and X with two lines
        //parameters: Point
        //return value: void
        public override void Draw(Point a)
        {
            Graphics g = graphics;
            Pen p = this.getPen();
            p.Color = Color.Orange;
            g.DrawLine(p, a.X, a.Y, a.X+75, a.Y+75);
            g.DrawLine(p, a.X, a.Y + 75, a.X + 75, a.Y);
        }
    }
}
