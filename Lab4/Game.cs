﻿//Description: Game class to handle the tracking of moves and checking for win conditions
//Auhtor: Barich, Alex
//Class: CS3160 Spring 2017 Sect 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    class Game
    {
        //track turns, areas for placement, and shapes
        private int mTurn;
        private Shape[,] grid;
        private Coordinates[,] coord;

        //function name: GetCoordinates
        //description: returns coord member
        //parameters: none
        //return value: Coordniates array
        public Coordinates[,] GetCoordinates()
        {
            return coord;
        }
        //function name: GetGrid
        //description: returns Grid member of class
        //parameters: none
        //return value: Shape array
        public Shape[,] GetGrid()
        {
            return this.grid;
        }
        //function name: SetGrid
        //description: Sets the boundaries based
        //              X and Y values and sets shape
        //parameters: x value and y value to determine quadrant
        //          and shape to save its value
        //return value: void
        public void SetGrid(int x, int y, Shape s)
        {
            //Q1
            if((x > 25 & x < 100) && (y > 100 && y < 175))
            {
                grid[0, 0] = s;
            }
            //Q2
            else if ((x > 100 & x < 200) && (y > 100 && y < 175))
            {
                grid[0, 1] = s;
            }
            //Q3
            else if ((x > 200 & x < 275) && (y > 100 && y < 175))
            {
                grid[0, 2] = s;
            }
            //Q4
            else if ((x > 25 & x < 100) && (y > 175 && y < 275))
            {
                grid[1, 0] = s;
            }
            //Q5
            else if ((x > 100 & x < 200) && (y > 175 && y < 275))
            {
                grid[1, 1] = s;
            }
            //Q6
            else if ((x > 200 & x < 275) && (y > 175 && y < 275))
            {
                grid[1, 2] = s;
            }
            //Q7
            else if ((x > 25 & x < 100) && (y > 275 && y < 350))
            {
                grid[2, 0] = s;
            }
            //Q8
            else if ((x > 100 & x < 200) && (y > 275 && y < 350))
            {
                grid[2, 1] = s;
            }
            //Q9
            else if ((x > 200 & x < 275) && (y > 275 && y < 350))
            {
                grid[2, 2] = s;
            }
        }
        //function name: Moves
        //description: determines who's turn it is based on mTurn
        //parameters: none
        //return value: mTurn integer
        public int Moves { get; set; }
        //function name: resetGrid
        //description: Disposes of all shapes in grid
        //parameters: none
        //return value: void
        private void resetGrid()
        {
            //traverse grid and dispose of each element
            foreach(Shape s in grid)
            {
                s.Dispose();
            }
        }
        //function name: Game
        //description: Game Constructor that instantiates
        //              mTurn to an odd value, coord, and grid
        //parameters: none
        //return value: none
        public Game()
        {
            mTurn = 3;
            this.Moves = mTurn;
            coord = new Coordinates[3, 3];
            grid = new Shape[3, 3];

            coord[0,0] = new Coordinates(25,100);
            coord[0, 1] = new Coordinates(100, 100);
            coord[0, 2] = new Coordinates(200, 100);
            coord[1, 0] = new Coordinates(25, 175);
            coord[1, 1] = new Coordinates(100, 175);
            coord[1, 2] = new Coordinates(200, 175);
            coord[2, 0] = new Coordinates(25, 275);
            coord[2, 1] = new Coordinates(100, 275);
            coord[2, 2] = new Coordinates(200, 275);
            
        }
        //function name: NewGame()
        //description: A pointless class that I'm really confused why it returns and integer
        //parameters: none
        //return value: string
        public string NewGame()
        {
            return "true";
        }
        //function name: IsGameOver
        //description: a very tedious process of checking for a full board state
        //parameters: none
        //return value: bool
        public bool IsGameOver()
        {
            //a bool value to track each quadrant
            bool Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Over;
            Q1 = Q2 = Q3 = Q4 = Q5 = Q6 = Q7 = Q8 = Q9 = Over = false;
            //check each quadrant and set value
            if (grid[0, 0] != null)
            {
                if (grid[0, 0].GetType() == typeof(XShape) || grid[0, 0].GetType() == typeof(OShape))
                {
                    Q1 = true;
                }
            }
            if (grid[0, 1] != null)
            {
                if (grid[0, 1].GetType() == typeof(XShape) || grid[0, 1].GetType() == typeof(OShape))
                {
                    Q2 = true;
                }
            }
            if (grid[0, 2] != null)
            {
                if (grid[0, 2].GetType() == typeof(XShape) || grid[0, 2].GetType() == typeof(OShape))
                {
                    Q3 = true;
                }
            }
            if (grid[1, 0] != null)
            {
                if (grid[1, 0].GetType() == typeof(XShape) || grid[1, 0].GetType() == typeof(OShape))
                {
                    Q4 = true;
                }
            }
            if (grid[1, 1] != null)
            {
                if (grid[1, 1].GetType() == typeof(XShape) || grid[1, 1].GetType() == typeof(OShape))
                {
                    Q5 = true;
                }
            }
            if (grid[1, 2] != null)
            {
                if (grid[1, 2].GetType() == typeof(XShape) || grid[1, 2].GetType() == typeof(OShape))
                {
                    Q6 = true;
                }
            }
            if (grid[2, 0] != null)
            {
                if (grid[2, 0].GetType() == typeof(XShape) || grid[2, 0].GetType() == typeof(OShape))
                {
                    Q7 = true;
                }
            }
            if (grid[2, 1] != null)
            {
                if (grid[2, 1].GetType() == typeof(XShape) || grid[2, 1].GetType() == typeof(OShape))
                {
                    Q8 = true;
                }
            }
            if (grid[2, 2] != null)
            {
                if (grid[2, 2].GetType() == typeof(XShape) || grid[2, 2].GetType() == typeof(OShape))
                {
                    Q9 = true;
                }
            }
            //check if all are true and it is a tie / win
            if(Q1 == true && Q2 == true && Q3 == true && Q4 == true && Q5 == true && Q6 == true & Q7 == true && Q8 == true && Q9 == true)
            {
                Over = true;
            }

            return Over;
        }
        //function name: isWin
        //description: again another tedious task of checking for win conditions based on
        //              the user's positions in grid and returning a bool if true
        //parameters: none
        //return value: bool
        public bool isWin()
        {
            //create bools for each quadrant including both O and X quadrants
            bool Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, K1, K2, K3, K4, K5, K6, K7, K8, K9, Win;
            Q1 = Q2 = Q3 = Q4 = Q5 = Q6 = Q7 = Q8 = Q9 = K1 = K2 = K3 = K4 = K5 = K6 = K7 = K8 = K9 = Win = false;
            //check each grid coordinate
            if (grid[0, 0] != null)
            {
                if (grid[0, 0].GetType() == typeof(XShape))
                {
                    Q1 = true;
                }
            }
            if (grid[0, 1] != null)
            {
                if (grid[0, 1].GetType() == typeof(XShape))
                {
                    Q2 = true;
                }
            }
            if (grid[0, 2] != null)
            {
                if (grid[0, 2].GetType() == typeof(XShape))
                {
                    Q3 = true;
                }
            }
            if (grid[1, 0] != null)
            {
                if (grid[1, 0].GetType() == typeof(XShape))
                {
                    Q4 = true;
                }
            }
            if (grid[1, 1] != null)
            {
                if (grid[1, 1].GetType() == typeof(XShape))
                {
                    Q5 = true;
                }
            }
            if (grid[1, 2] != null)
            {
                if (grid[1, 2].GetType() == typeof(XShape))
                {
                    Q6 = true;
                }
            }
            if (grid[2, 0] != null)
            {
                if (grid[2, 0].GetType() == typeof(XShape))
                {
                    Q7 = true;
                }
            }
            if (grid[2, 1] != null)
            {
                if (grid[2, 1].GetType() == typeof(XShape))
                {
                    Q8 = true;
                }
            }
            if (grid[2, 2] != null)
            {
                if (grid[2, 2].GetType() == typeof(XShape))
                {
                    Q9 = true;
                }
            }

            if (grid[0, 0] != null)
            {
                if (grid[0, 0].GetType() == typeof(OShape))
                {
                    K1 = true;
                }
            }
            if (grid[0, 1] != null)
            {
                if (grid[0, 1].GetType() == typeof(OShape))
                {
                    K2 = true;
                }
            }
            if (grid[0, 2] != null)
            {
                if (grid[0, 2].GetType() == typeof(OShape))
                {
                    K3 = true;
                }
            }
            if (grid[1, 0] != null)
            {
                if (grid[1, 0].GetType() == typeof(OShape))
                {
                    K4 = true;
                }
            }
            if (grid[1, 1] != null)
            {
                if (grid[1, 1].GetType() == typeof(OShape))
                {
                    K5 = true;
                }
            }
            if (grid[1, 2] != null)
            {
                if (grid[1, 2].GetType() == typeof(OShape))
                {
                    K6 = true;
                }
            }
            if (grid[2, 0] != null)
            {
                if (grid[2, 0].GetType() == typeof(OShape))
                {
                    K7 = true;
                }
            }
            if (grid[2, 1] != null)
            {
                if (grid[2, 1].GetType() == typeof(OShape))
                {
                    K8 = true;
                }
            }
            if (grid[2, 2] != null)
            {
                if (grid[2, 2].GetType() == typeof(OShape))
                {
                    K9 = true;
                }
            }

            //check for horizontal diagonal and verticle wins
            if((Q1 == true && Q2 == true && Q3 == true) || (K1 == true && K2 == true && K3 == true))
            {
                Win = true;
            }
            else if((Q4 == true && Q5 == true && Q6 == true) || (K4 == true && K5 == true && K6 == true))
            {
                Win = true;
            }
            else if ((Q7 == true && Q8 == true && Q9 == true) || (K7 == true && K8 == true && K9 == true))
            {
                Win = true;
            }
            else if ((Q1 == true && Q4 == true && Q7 == true) || (K1 == true && K4 == true && K7 == true))
            {
                Win = true;
            }
            else if ((Q2 == true && Q5 == true && Q8 == true) || (K2 == true && K5 == true && K8 == true))
            {
                Win = true;
            }
            else if ((Q3 == true && Q6 == true && Q9 == true) || (K3 == true && K6 == true && K9 == true))
            {
                Win = true;
            }
            else if ((Q1 == true && Q5 == true && Q9 == true) || (K1 == true && K5 == true && K9 == true))
            {
                Win = true;
            }
            else if ((Q3 == true && Q5 == true && Q7 == true) || (K3 == true && K5 == true && K7 == true))
            {
                Win = true;
            }
            else
            {
                Win = false;
            }
            return Win;
        }
        //function name: NextMove
        //description: Increments mTurn and updates whos turn it is
        //parameters: none
        //return value: string
        public string NextMove()
        {
            //check for even value, if so O else odd and X
            if(this.Moves % 2 == 0)
            {
                this.Moves = this.Moves + 1;
                return "O's Turn";
            }
            this.Moves = this.Moves + 1;
            return "X's Turn";
        }
    }
}
